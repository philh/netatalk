Source: netatalk
Section: net
Priority: optional
Maintainer: Debian Netatalk team <pkg-netatalk-devel@lists.alioth.debian.org>
Uploaders:
 Daniel Markstedt <daniel@mindani.net>,
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 bison,
 cmark-gfm <!nodoc>,
 debhelper-compat (= 13),
 docbook-xsl,
 flex,
 libacl1-dev,
 libavahi-client-dev,
 libcrack2-dev,
 libdb-dev,
 libdbus-glib-1-dev,
 libevent-dev,
 libgcrypt20-dev,
 libkrb5-dev,
 libldap2-dev,
 libltdl3-dev,
 libmariadb-dev-compat,
 libpam0g-dev,
 libssl-dev,
 libtalloc-dev,
 libtdb-dev,
 libtirpc-dev,
 libtracker-sparql-3.0-dev,
 libwrap0-dev,
 systemtap-sdt-dev,
 tracker,
 unicode-data,
 xsltproc,
Build-Conflicts:
 libavahi-compat-libdnssd-dev,
Vcs-Git: https://salsa.debian.org/netatalk-team/netatalk.git
Vcs-Browser: https://salsa.debian.org/netatalk-team/netatalk
Standards-Version: 4.7.0
Homepage: https://github.com/Netatalk/netatalk
Rules-Requires-Root: no

Package: netatalk
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libpam-modules,
 netbase,
 ${misc:Depends},
 ${perl:Depends},
 ${shlibs:Depends},
Recommends:
 avahi-daemon,
 cracklib-runtime,
 dbus,
 libio-socket-ip-perl,
 libnet-dbus-perl,
 lsof,
 perl,
 procps,
 tracker (>= 3.0),
 tracker-miner-fs (>= 3.0),
Suggests:
 quota,
Description: Apple Filing Protocol service
 Netatalk is an implementation of the Apple Filing Protocol (AFP),
 for offering file service (mainly) to macOS clients.
 .
 Compared to the Windows-native SMB protocol
 (which is also supported on macOS),
 the Macintosh-native AFP protocol is accessible from a different network,
 can be simpler to setup and faster for some operations
 (e.g. server-to-server copying),
 and some features (e.g. the backup service Time Machine)
 may work more reliably than over SMB.
 .
 This package contains all daemon and utility programs
 as well as Netatalk's libraries.
